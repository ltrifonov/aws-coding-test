## AE AWS Coding Test

## Notes

The project was implemented using the follwing considerations:

 - I've used `Terraform v0.10.7`; 
 - the default region specified is Sydney - `ap-southeast-2`;
 - Default VPC must exist in the region. A dedicated data resource exists, which will fail at plan stage if there is no Default VPC;
 - Please provide a region with default VPC, as VPC management is not requested in the task. The default region can be chaged by editing the value of:

```
variable "region" {
  default = "ap-southeast-2"
}
```

- To build in different region, please add `-var 'region=us-west-2'` or another to the command below;
- To simplify the deployment process, I've selected Amazon Linux instance. It has aws cli installed by default and free. AWS web site states: 
> The Amazon Linux AMI is provided at no additional charge to Amazon EC2 users

- The project is as minimalistic as possible, hence no Security groups, subnets, loadbalancers, or even ssh keys provided;
- All credentials and the s3 bucket name must be provided at PLAN stage from the command line, as requested:

```
terraform init
terraform plan -out l1.tfplan -var 'bucket_name=unique-s3bucket-concourse-asg' -var 'aws_secret_access_key=yyyyyyy' -var 'aws_access_key_id=XXX'
```
 - the plan will report 6 resources need to be created:

```
  + aws_autoscaling_group.concourse
  + aws_iam_instance_profile.main
  + aws_iam_role.main
  + aws_iam_role_policy.concourse
  + aws_launch_configuration.concourse
  + aws_s3_bucket.concourse
```

 - terrform doesn't allow passing variables at apply stage. Hence, the example in the requirements document is little misleading;
 - after the plan is executed, the command to build the setup is:

```
terraform apply l1.tfplan
```

- the selected instance type is `t2.micro`, which qualifies for the free tier;
- the userdata is minimalistic, however, it will output all userdata commands to the AWS System log, as no other method of debug is possible;

![System Output](out.png)  

*Lyubomir Trifonov, 31 Oct 2017*
 

