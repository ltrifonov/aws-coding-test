## variables
variable "name" {
  default = "AeAWSCoding"
}

variable "region" {
  default = "ap-southeast-2"
}

variable "author" {
  default = "LTrifonov"
}

variable "aws_access_key_id" {
  default = ""
}

variable "aws_secret_access_key" {
  default = ""
}

variable "bucket_name" {}

#provider devinition
provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
  region     = "${var.region}"
}

#check if the default vpc exists in the region. Fail otherwise
data "aws_vpc" "default" {
  default = true
}

#userdata
data "template_file" "userdata" {
  template = <<EOF
#!/bin/bash -x

exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

echo "==========================================" > /tmp/output.txt
echo "The output is:" >> /tmp/output.txt
echo "  ami_id: `curl http://169.254.169.254/latest/meta-data/ami-id`" >> /tmp/output.txt
echo "  local host name: `curl http://169.254.169.254/latest/meta-data/local-hostname`" >> /tmp/output.txt
echo "  public host name: `curl http://169.254.169.254/latest/meta-data/public-hostname`" >> /tmp/output.txt
echo >> /tmp/output.txt
echo "  author: $${theauthor}" >> /tmp/output.txt

aws --region $${awsregion} s3 cp /tmp/output.txt s3://$${s3bucket}/output.txt

EOF

  vars {
    s3bucket  = "${aws_s3_bucket.concourse.id}"
    theauthor = "${var.author}"
    awsregion = "${var.region}"
  }
}

#discover the latest AWS Linux ami
data "aws_ami" "awslinux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}

#launch config
resource "aws_launch_configuration" "concourse" {
  depends_on           = ["aws_s3_bucket.concourse"]
  image_id             = "${data.aws_ami.awslinux.id}"
  instance_type        = "t2.micro"
  iam_instance_profile = "${aws_iam_instance_profile.main.name}"
  user_data            = "${data.template_file.userdata.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

#autoscalling group
resource "aws_autoscaling_group" "concourse" {
  availability_zones        = ["${var.region}a", "${var.region}b"]
  name                      = "asg-${var.name}"
  max_size                  = 2
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.concourse.name}"

  timeouts {
    delete = "15m"
  }

  tag {
    key                 = "author"
    value               = "${var.author}"
    propagate_at_launch = true
  }

  tag {
    key                 = "name"
    value               = "test-asg-${var.name}"
    propagate_at_launch = true
  }
}

#s3 bucket
resource "aws_s3_bucket" "concourse" {
  bucket = "${var.bucket_name}"
  acl    = "private"
  region = "${var.region}"

  tags {
    role      = "${var.name}"
    author    = "${var.author}"
    terraform = "yes"
  }
}

#iam policy, roles and profiles
data "aws_iam_policy_document" "concourse" {
  statement {
    effect = "Allow"

    actions = [
      "s3:*",
    ]

    resources = [
      "${aws_s3_bucket.concourse.arn}/*",
      "${aws_s3_bucket.concourse.arn}",
    ]
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "main" {
  name               = "lyu-test-${var.name}"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role.json}"
}

resource "aws_iam_instance_profile" "main" {
  name = "lyu-test-${var.name}"
  role = "${aws_iam_role.main.name}"
}

resource "aws_iam_role_policy" "concourse" {
  name   = "iam-role-${var.name}"
  policy = "${data.aws_iam_policy_document.concourse.json}"
  role   = "${aws_iam_role.main.name}"
}
